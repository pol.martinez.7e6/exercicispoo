class Estudiant(private val nom : String, private val nota : String) {
    fun mostrarDades() {
        println("Nom: ${this.nom}, la seva nota és: ${this.nota}")
    }
}

class Arcoiris {
    private val colores = mutableListOf("rojo", "naranja", "amarillo", "verde", "cian", "azul", "violeta")

    fun pideColor(): Boolean {
        println("Escribe un color:")
        val color = readln()

        return color in colores
    }
}

open class Instruments {
    open fun makeSounds(times: Int) {}
}

class Drump(private val so: String) : Instruments() {
    override fun makeSounds(times: Int) {
        val sound = when (so.uppercase()) {
            "A" -> "TAAAM"
            "O" -> "TOOOM"
            "U" -> "TUUUM"
            else -> {}
        }
        repeat(times) {
            println(sound)
        }
    }
}

class Triangle(private val resonancia: Int) : Instruments() {
    override fun makeSounds(times: Int) {
        val sound = "T" + "I".repeat(resonancia) + "NC"
        repeat(times) {
            println(sound)
        }
    }
}

fun play(instruments: List<Instruments>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}

open class Question(val preguntes: String, val resposta: String)

class FreeTextQuestion(preguntes: String, resposta: String) : Question(preguntes, resposta)

class MultipleChoiceQuestion(preguntes: String, val opcions: List<String>, resposta: String) : Question(preguntes, resposta)

class Quiz(private val preguntes: List<Question>) {
    fun mostraQuiz(quiz: Quiz) {
        var preguntesNumber = 1
        for (preguntes in quiz.preguntes) {
            println("Preguntes $preguntesNumber: ${preguntes.preguntes}")
            when (preguntes) {
                is MultipleChoiceQuestion -> {
                    println("Escull:")
                    for (choice in preguntes.opcions) {
                        println("- $choice")
                    }
                }
            }
            preguntesNumber++
        }
    }

    fun jugaQuiz(quiz: Quiz) {
        var score = 0
        var preguntesNumber = 1
        for (preguntes in quiz.preguntes) {
            print("Preguntes $preguntesNumber: ${preguntes.preguntes} ")
            when (preguntes) {
                is FreeTextQuestion -> {
                    println()
                    print("Resposta: ")
                    val userAnswer = readln()
                    if (userAnswer == preguntes.resposta) {
                        println("Correcte!")
                        score++
                    } else {
                        println("Incorrecte. La resposta és: ${preguntes.resposta}")
                    }
                }
                is MultipleChoiceQuestion -> {
                    println()
                    for ((index, choice) in preguntes.opcions.withIndex()) {
                        println("${index + 1}. $choice")
                    }
                    print("Resposta: ")
                    val userChoice = readln()
                    if (userChoice == preguntes.resposta) {
                        println("Correcte!")
                        score++
                    } else {
                        println("Incorrecte. La resposta és: ${preguntes.resposta}")
                    }
                }
            }
            preguntesNumber++
        }
        println("Has obtingut una puntuació de $score sobre ${quiz.preguntes.size} preguntes correctes.")
    }
}

fun main() {
    val e1 = Estudiant("Pol", "Bé")
    val e2 = Estudiant("Oriol", "Suspès")

    e1.mostrarDades()
    e2.mostrarDades()

    val arcoiris = Arcoiris()
    println(arcoiris.pideColor())

    val instruments: List<Instruments> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)

    val quiz = Quiz(
        listOf(
            FreeTextQuestion("Quina és la capital d'Espanya", "Madrid"),
            MultipleChoiceQuestion(
                "Quina es la montanya més alta del món?",
                listOf("Kilimanjaro", "Everest", "Fuji"),
                "Everest"
            ),
            FreeTextQuestion("De quin color es el cel?", "Blau")
        )
    )
    quiz.mostraQuiz(quiz)
    quiz.jugaQuiz(quiz)
}